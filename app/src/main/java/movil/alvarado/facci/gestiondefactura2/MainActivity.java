package movil.alvarado.facci.gestiondefactura2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        Log.e( "4B", "OnCreate" );
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.e( "4B", "OnStart" );
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e( "4B", "OnResume" );
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.e( "4B", "OnRestart" );
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.e( "4B", "OnPause" );

    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.e( "4B", "OnStop" );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.e( "4B", "OnDestroy" );
    }
}


